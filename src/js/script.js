$(document).ready(function() {
    $("#twitter-slider").owlCarousel({
      items: 1,
      loop: true,
      autoplay: true,
      autoplayTimeout: 4000
    });


    $('.search-toggle').on('click', function(){
        $('.search-box').addClass('active');
    });

    $('.close-search').on('click', function(){
        $('.search-box').removeClass('active');
    });

    $('#menu-toggle').on('click', function(){
        $('#mobile-menu-links').addClass('active');
    });

    $('.menu-close').on('click', function(){
        $('#mobile-menu-links').removeClass('active');
    });
});
